# ⚠️⚠️ DEPRECATED ⚠️⚠️

This project has been deprecated/archived since we have created a real Capacitor plugin for this and open-sourced it over here - https://github.com/mediagrid/capacitor-native-audio.

# DISCLAIMER

Feel free to use (copy and paste) the code from this repo. However, it is provided AS-IS. Use at your own risk. No support will be provided.

# Description

This sample code is a Capacitor ["local plugin"](https://capacitorjs.com/docs/plugins/tutorial/introduction) that allows for playing audio on Android and iOS in a Capacitor app.

The motivation to create a custom plugin was that all existing plugins didn't work for our use case which was playing audio with background music (with volume control of the music).

## Features

-   Plays audio from a URL (doesn't support local files)
-   Will play in the background when the app is not in the foreground/active
-   Allows for playing background music while another audio file is playing
-   Displays media controls on lock screen/notification bar
-   Handles audio interruptions from other apps

## Known issues

-   On Android, uses Google's [ExoPlayer](https://github.com/google/ExoPlayer) which has been replaced by [AndroidX Media](https://github.com/androidx/media)
-   On Android (at least), can't control with bluetooth devices
-   On Android (at least), background music doesn't stop on a longer audio file until the app is active again. This is because it was set up to control the background music from the JavaScript and on Android JS stops running at some point when the app is in the background. The solution is to move the stop logic to the native code.
-   There is no web support, it will throw an error.
-   On iOS, when multiple audio tracks have played and there is an interruption from GPS let's say, old audio tracks start playing when audio is focused again. Possible cause is because the audio playlist is not being cleared when an audio track ends.

# Android Setup

Where `your.app.path` is referenced, replace with your own App ID.

## Add ExoPlayer dependencies

```gradle
// android/app/build.gradle

dependencies {
    // OTHER STUFF
    implementation 'com.google.android.exoplayer:exoplayer-core:2.18.1'
    implementation 'com.google.android.exoplayer:exoplayer-ui:2.18.1'
}
```

## Register plugin

```java
// android/app/src/main/java/your/app/path/MainActivity.java

import your.app.path.audio.AudioPlayerPlugin;

public class MainActivity extends BridgeActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        registerPlugin(AudioPlayerPlugin.class); // Register here!

        super.onCreate(savedInstanceState);
    }
}
```

## Register `strings.xml`

```xml
<!-- android/app/src/main/res/values/strings.xml -->

<?xml version='1.0' encoding='utf-8'?>
<resources>
    <!-- OTHER STUFF -->
    <string name="audio_player_service_description">Allows for audio to play in the background.</string>
</resources>

```

## Modify `AndroidManifest.xml`

Register the audio service and add the foreground service permission.

```xml
<!-- android/app/src/main/AndroidManifest.xml -->

<manifest xmlns:android="http://schemas.android.com/apk/res/android">
    <application>
    <!-- OTHER STUFF -->

    <service
        android:name=".audio.AudioPlayerService"
        android:description="@string/audio_player_service_description"
    />

    <!-- OTHER STUFF -->
    </application>

    <uses-permission android:name="android.permission.FOREGROUND_SERVICE" />
</manifest>
```

# iOS

## Enable Audio Background Mode

This can be done in XCode or by editing `Info.plist` directly.

```xml
<!-- ios/App/App/Info.plist -->

<dict>
    <!-- OTHER STUFF -->

    <key>UIBackgroundModes</key>
    <array>
        <string>audio</string>
    </array>

    <!-- OTHER STUFF -->
</dict>
```
