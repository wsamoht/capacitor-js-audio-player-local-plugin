package your.app.path.audio;

public class DestroyNotAllowedException extends Exception {
    public DestroyNotAllowedException(String message) {
        super(message);
    }
}
